package es.cipfpbatoi.gestorusuarios.controllers;

import es.cipfpbatoi.gestorusuarios.exceptions.NotFoundException;
import es.cipfpbatoi.gestorusuarios.model.entities.User;
import es.cipfpbatoi.gestorusuarios.model.repositories.UserRepository;
import es.cipfpbatoi.gestorusuarios.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    private static final DateTimeFormatter ENGLISH_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    private UserRepository userRepository;

    @GetMapping(value = "/users")
    public String userListAction(Model model, @RequestParam(required = false) String search) {
        List<User> users;
        if (search != null && !search.isBlank() && search.length() >= 2) {
            users = userRepository.findAll(search);
        } else {
            users = userRepository.findAll();
        }
        model.addAttribute("users", users);
        return "user-list-view";
    }

    @GetMapping(value = "/user-add")
    public String addUserFormAction() {
        return "user-add-form";
    }

    @GetMapping(value = "/user-detail")
    public String addUserFormAction(String dni, Model model) throws NotFoundException {
        User user = userRepository.getById(dni);
        model.addAttribute("user", user);
        return "user-detail-form";
    }

    @PostMapping(value = "/user-add")
    public String postAddUser(@RequestParam Map<String, String> params,
                              RedirectAttributes redirectAttributes) {
        String name = params.get("name");
        String surname = params.get("surname");
        String email = params.get("email");
        String dni = params.get("dni");
        String mobilePhone = params.get("telephoneNumber");
        String birthday = params.get("birthday");
        String zipCode = params.get("zipCode");
        String password1 = params.get("password1");
        String password2 = params.get("password2");

        HashMap<String, String> errors = new HashMap<>();
        if (!Validator.hasLength(name, 3)) {
            errors.put("name", "Debe tener, al menos 3 caracteres");
        }
        if (!Validator.hasLength(surname, 3)) {
            errors.put("surname", "Debe tener, al menos 3 caracteres");
        }
        if (!Validator.isValidEmail(email)) {
            errors.put("email", "No es un email válido");
        }
        if (!Validator.isValidDNI(dni)) {
            errors.put("dni", "No es un dni válido");
        }
        if (!Validator.isValidMobilePhone(mobilePhone)) {
            errors.put("mobilePhone", "No es un movil válido");
        }
        if (!Validator.isValidDate(birthday)) {
            errors.put("birthday", "No es un dni válido");
        }
        if (!Validator.isValidZipCode(zipCode)) {
            errors.put("zip", "No es un código postal español válido");
        }
        if (!Validator.isValidPassword(password1)) {
            errors.put("password", "Debe tener una longitud entre 5 y 20 caracteres y contener al menos letras mayúsculas, minúsculas y un carácter especial");
        }
        if(!password1.equals(password2)) {
            errors.put("repeat_password", "Las contraseñas introducidas no coinciden");
        }
        if (errors.size() > 0) {
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:/user-add";
        }
        LocalDate birthdayDate = LocalDate.parse(birthday, ENGLISH_DATE_FORMATTER);

        User user = new User(name, surname, dni, email, zipCode, mobilePhone, birthdayDate, password1);
        userRepository.add(user);
        redirectAttributes.addFlashAttribute("infoMessage", "Usuario insertado con éxito");
        redirectAttributes.addAttribute("dni", dni);
        return "redirect:/user-detail";
    }
}
