package es.cipfpbatoi.gestorusuarios.model.repositories;

public interface UserInterface {

    String getPassword();

    String getSalt();

    String getUserName();

}
