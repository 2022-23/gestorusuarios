package es.cipfpbatoi.gestorusuarios.model.repositories;

import es.cipfpbatoi.gestorusuarios.exceptions.NotFoundException;
import es.cipfpbatoi.gestorusuarios.model.entities.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;

@Service
public class UserRepository {
    private ArrayList<User> users;

    public UserRepository() {
        users = new ArrayList<>();
        setDefaultUsers();
    }

    public void add(User usuario) {
        users.add(usuario);
    }

    public ArrayList<User> findAll() {
        return users;
    }

    public ArrayList<User> findAll(String searchField) {
        ArrayList<User> filteredUsers = new ArrayList<>();
        for (User item : users) {
            if (item.getName().startsWith(searchField)
                    || item.getSurname().contains(searchField)) {
                filteredUsers.add(item);
            }
        }
        return filteredUsers;
    }

    public User getById(String dni) throws NotFoundException {
        User usuario = findById(dni);
        if (usuario != null) {
            return usuario;
        }
        throw new NotFoundException("User", dni);
    }

    private User findById(String dni) {
        User searchedUser = new User(dni);
        if (users.contains(searchedUser)) {
            return users.get(users.indexOf(searchedUser));
        }
        return null;
    }

    public void remove(User user) throws NotFoundException {
        users.remove(user);
    }

    private void setDefaultUsers() {
        users.add(new User("Batoi", "surname 1 surname 2", "00000000A", "test@test.es", "03801", "+340000001",
                LocalDate.of(1990,1,12), "0123456789"));
        users.add(new User("Informática", "surname 2 surname 3", "00000000B", "test1@test.es", "03801", "+340000002",
                LocalDate.of(1990,2,12), "0123456789"));
    }
}
